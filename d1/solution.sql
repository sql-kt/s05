--1
SELECT customerName FROM customers WHERE country = "philippines";
--2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "la rochelle gifts";
--3
SELECT productName, MSRP FROM products WHERE productName = "the titanic";
--4
SELECT lastName, firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
--5
SELECT customerName FROM customers WHERE state IS NULL;
--6
SELECT lastName, firstName, email FROM employees WHERE (lastName = "Patterson" AND firstName = "Steve");
--7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;
--8
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";
--9
SELECT productLine FROM productLines WHERE textDescription LIKE "%state of the art%";
--10
SELECT DISTINCT country FROM customers;
--11
SELECT DISTINCT status FROM orders;
--12
SELECT customerName FROM customers WHERE country IN ("USA", "France", "Canada");
--13
SELECT employees.firstName, employees.lastName, offices.city FROM employees
JOIN offices ON employees.officeCode WHERE offices.city = "Tokyo";
--14
SELECT customers.customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";
--15
SELECT products.productName FROM products JOIN orderdetails JOIN orders JOIN customers ON (products.productCode = orderdetails.productCode AND orderdetails.orderNumber = orders.orderNumber AND orders.customerNumber = customers.customerNumber) WHERE customers.customerName = "Baane Mini Imports";
--16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees JOIN offices JOIN customers ON (employees.employeeNumber = customers.salesRepEmployeeNumber AND employees.officeCode = offices.officeCode) WHERE customers.country = offices.country;
--17
SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;
--18
SELECT customerName, phone from customers WHERE phone LIKE "%+81%";
